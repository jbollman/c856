from flask import Flask, render_template
app = Flask(__name__, static_url_path='/static')
app.static_folder = 'static'



@app.route("/")
@app.route("/home")  #this allows multiple routes, to go to same end destination
def home():
    return render_template('home.html')


@app.route("/about")
def about():
    return render_template('about.html')

@app.route("/accomodations")
def accomodations():
    return render_template('accomodations.html')

@app.route("/activities")
def activities():
    return render_template('activities.html')


@app.route("/contact")
def contact():
    return render_template('contact.html')

@app.route("/cusine")
def cusine():
    return render_template('cusine.html')

@app.route("/events")
def events():
    return render_template('events.html')


@app.route("/faq")
def faq():
    return render_template('faq.html')


@app.route("/history")
def history():
    return render_template('history.html')


@app.route("/photos")
def photos():
    return render_template('photos.html')



@app.route("/transportation")
def transportation():
    return render_template('transportation.html')